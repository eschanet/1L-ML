#!/usr/bin/env python

from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import train_test_split
from sklearn.metrics import classification_report,confusion_matrix

import numpy as np
import argparse, os

from keras.models import Sequential
from keras.layers import Dense, Dropout, Flatten
from keras.callbacks import EarlyStopping

import matplotlib.pyplot as plt
import vars as v
import plot as p
from commonHelpers.logger import logger
logger = logger.getChild("train")

from root_numpy import root2array, tree2array, rec2array

from sklearn.neural_network import MLPClassifier
from sklearn.metrics import roc_curve, auc, confusion_matrix

#np.set_printoptions(threshold=np.nan)

def trainModel(X_train,y_train,w_train,X_test,y_test,w_test,X_val,y_val,w_val,class_weight):
    
    model = Sequential()
    model.add(Dense(32, input_dim=len(v.variables), activation='relu'))
    #model.add(Dropout(0.5))
    model.add(Dense(32, activation='relu'))
    #model.add(Dropout(0.5))
    model.add(Dense(32, activation='relu'))
    #model.add(Dropout(0.5))
    model.add(Dense(1, activation='sigmoid'))
    
    logger.info("Compile model.")
    model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])
        
    logger.info("Training model.")
    
    # Fit the model
    history = model.fit(X_train, y_train, epochs=20, validation_data=(X_val, y_val),batch_size=500,class_weight=class_weight)
    
    return model,history

def predictWithModel(model, X_test, X_train):

    logger.info('Predicting.')
    test_probabilities = model.predict(X_test)
    train_probabilities = model.predict(X_train)
    
    return test_probabilities, train_probabilities
    
    
def constructNumpyArrays(bkg,sig, weight_expr,args):
    
    # Split data into testing and training for bkg and sig (with weights still inside array)
    sig_train, sig_test = train_test_split(sig, test_size = args.test_size, random_state=1234)
    bkg_train, bkg_test = train_test_split(bkg, test_size = args.test_size, random_state=1234)  
    
    sig_train, sig_val = train_test_split(sig_train, test_size = 0.3, random_state=1234)
    bkg_train, bkg_val = train_test_split(bkg_train, test_size = 0.3, random_state=1234)  
    
    X_train = rec2array(sig_train[v.variables])
    X_train = np.concatenate((X_train , rec2array(bkg_train[v.variables]) ))
    
    X_test = rec2array(sig_test[v.variables])
    X_test = np.concatenate((X_test, rec2array(bkg_test[v.variables])))
    
    X_val = rec2array(sig_val[v.variables])
    X_val = np.concatenate((X_val, rec2array(bkg_val[v.variables])))
    
    logger.info("Number of events in X_train: {}".format(X_train.shape[0]))
    logger.info("Number of variables in X_train: {}".format(X_train.shape[1]))

    logger.info("Number of events in X_test: {}".format(X_test.shape[0]))
    logger.info("Number of variables in X_test: {}".format(X_test.shape[1]))
    
    logger.info("Number of events in X_val: {}".format(X_val.shape[0]))
    logger.info("Number of variables in X_val: {}".format(X_val.shape[1]))

    w_train = sig_train[weight_expr]
    w_train = np.concatenate((w_train, bkg_train[weight_expr]))
    w_train = w_train.reshape((w_train.shape[0],1))
    
    w_test = sig_test[weight_expr]
    w_test = np.concatenate((w_test, bkg_test[weight_expr]))
    w_test = w_test.reshape((w_test.shape[0],1))
    
    w_val = sig_val[weight_expr]
    w_val = np.concatenate((w_val, bkg_val[weight_expr]))
    w_val = w_val.reshape((w_val.shape[0],1))

    y_train = np.concatenate((np.ones(sig_train.shape[0], dtype=np.uint8), np.zeros(bkg_train.shape[0], dtype=np.uint8)))
    y_test = np.concatenate((np.ones(sig_test.shape[0] , dtype=np.uint8), np.zeros(bkg_test.shape[0] , dtype=np.uint8)))
    y_val = np.concatenate((np.ones(sig_val.shape[0] , dtype=np.uint8), np.zeros(bkg_val.shape[0] , dtype=np.uint8)))
    
    if not y_train.shape[0] == X_train.shape[0]:
        raise ValueError('ERROR:      y_train has not the same size as X_train')
    if not y_test.shape[0] == X_test.shape[0]:
        raise ValueError('ERROR:      y_test has not the same size as X_test')
    if not y_train.shape[0] == w_train.shape[0]:
        raise ValueError('ERROR:      y_train has not the same size as w_train')
    if not y_test.shape[0] == w_test.shape[0]:
        raise ValueError('ERROR:      y_test has not the same size as w_test')
    logger.info('Data and label arrays have same size')
    
    imbalance = bkg.size/float(sig.size)
    logger.info("Imbalance between bkg and sig is {}".format(imbalance))

    scaler = StandardScaler()
    X_train = scaler.fit_transform(X_train)
    X_val = scaler.fit_transform(X_val)

    X_test = scaler.fit_transform(X_test)
    
    sumw_bkg = np.sum(w_train[y_train == 0])
    sumw_sig = np.sum(w_train[y_train == 1])
    class_weight = [(sumw_sig+sumw_bkg)/(2*sumw_bkg), (sumw_sig+sumw_bkg)/(2*sumw_sig)]
    logger.info("Class weight: {}".format(class_weight))
    
    return X_train,y_train,w_train,X_test,y_test,w_test,X_val,y_val,w_val,class_weight
    
    
def readRecArrays(args):
    
    # Load background numpy rec arrays and transform to unstructured array
    _bkg = []
    for name in ["ttbar"]:
        filename = args.i+name+'.npy'
        background = np.load(filename)
        _bkg.append(background)
    
    # Build total background array
    bkg = np.concatenate([_bkg[i] for i in range(len(_bkg))])

    # Load signal numpy rec array and transform to unstructured array
    signal = args.i+'C1C1_WW_400p0_0p0.npy'
    sig = np.load(signal)
    
    return bkg,sig,
    
def main():
    parser = argparse.ArgumentParser(description=("Train different ML models"))
    parser.add_argument("-i", default="/project/etp4/eschanet/1L/numpy/")
    parser.add_argument("-o", default="/project/etp4/eschanet/1L/numpy/")
    parser.add_argument("-test_size", default=0.25)
    parser.add_argument("-d", default='default')
    
    args = parser.parse_args()
    args.test_size = float(args.test_size)
    args.d = 'run/' + args.d + '/'
    args.d = args.d.replace('//','/')
    
    plotdir = args.d + 'plots/'
    if not os.path.exists(plotdir):
        os.makedirs(plotdir)
        
    bkg,sig = readRecArrays(args)
    
    weight_expr = "*".join(v.weights)
    logger.info("Weight expression is: {}".format(weight_expr))
    
    X_train,y_train,w_train,X_test,y_test,w_test,X_val,y_val,w_val,class_weight = constructNumpyArrays(bkg,sig, weight_expr,args)
    
    p.plotDistribution(v.variables,X_train,y_train,w_train,class_weight,args.d,False)

    model,history = trainModel(X_train,y_train,w_train,X_test,y_test,w_test,X_val,y_val,w_val,class_weight)
    
    test_probabilities, train_probabilities = predictWithModel(model, X_test, X_train)
    
    model.save(args.d+'dnn.h5')
    
    predictions = [float(round(x)) for x in test_probabilities]

    rounded = [round(x[0]) for x in test_probabilities]
    train_rounded = [round(x[0]) for x in train_probabilities]
    
    
    logger.info("Rounded test probabilities: ")

    checklist =  np.equal(rounded, y_test)
    sig_checklist = checklist[y_test==1]
    bkg_checklist = checklist[y_test==0]
    #sig_train_checklist =  np.equal(train_rounded, y_train[])
    #bkg_train_checklist =  np.equal(train_rounded, y_train)

    y_sig = 0
    y_bkg = 0
    for i in bkg_checklist:
        if i: y_bkg += 1
    for l in sig_checklist:
        if l: y_sig += 1
    logger.info("Correct bkg_test predictions: %i of %i ---> %.1f%%"%(y_bkg, X_test[y_test==0].shape[0], float(y_bkg)/X_test[y_test==0].shape[0]*100))
    logger.info("Correct sig_test predictions: %i of %i ---> %.1f%%"%(y_sig, X_test[y_test==1].shape[0], float(y_sig)/X_test[y_test==1].shape[0]*100))
    
    logger.info('Plotting ROC curve.')
    pltname = 'ROC_curve'
    
    p.plotROC(y_test, test_probabilities, pltname, args.d, False)
    
    p.plotOutputScore(X_test,y_test, test_probabilities,w_test,X_train,y_train,train_probabilities,w_train, class_weight, 'output_score', args.d,[20,0,1], False)
    p.plotAccuracy(history,args.d,'accuracy')
    p.plotLoss(history,args.d,'loss')


if __name__ == "__main__":
  main()