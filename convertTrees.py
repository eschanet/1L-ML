#!/usr/bin/env python

import argparse
from itertools import chain 

import numpy as np
import ROOT
from root_numpy import root2array, tree2array

import vars as v
import trees as t
from commonHelpers.logger import logger
logger = logger.getChild("convert")


def main():
    
    parser = argparse.ArgumentParser(description=("Convert ROOT files to numpy arrays with root_numpy"))
    parser.add_argument("-input-background", default="/project/etp5/SUSYInclusive/trees/v0-4/allTrees_v0_4_bkg.root")
    parser.add_argument("-input-signal", default="/project/etp5/SUSYInclusive/trees/v0-4/allTrees_v0_4_signal.root")
    parser.add_argument("-o", default="/project/etp4/eschanet/1L/numpy/")
    parser.add_argument("-cut", default=None)
    parser.add_argument("-step", default=None)
    parser.add_argument("-stop", default=None)
    args = parser.parse_args()
    
    logger.info("Input is {}".format(args.input_background))
    logger.info("Output is {}".format(args.o))
    step=None
    if not args.step is None:
        logger.info("Step is {}".format(int(args.step)))
        step=int(args.step)
    stop=None
    if not args.stop is None:
        logger.info("Stop is {}".format(int(args.stop)))
        stop=int(args.stop)
    weight_expr = "*".join(v.weights)
    for dict in chain(t.background, t.signal):
        tree = dict['name']
        logger.info("Converting " + tree) 
        if dict in t.signal:
            arr = root2array(args.input_signal, tree+'_NoSys', branches=v.variables+[weight_expr], selection=args.cut, step=step, stop=stop)
        else:
            arr = root2array(args.input_background, tree+'_NoSys', branches=v.variables+[weight_expr], selection=args.cut, step=step, stop=stop)
        outname = args.o+tree
        np.save(outname, arr)
    logger.info("Done.")

if __name__ == "__main__":
  main()
