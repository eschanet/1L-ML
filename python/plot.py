#!/usr/bin/env python

import numpy as np
from sklearn.metrics import roc_curve, auc, confusion_matrix
import matplotlib.pyplot as plt

import ROOT
from root_numpy import root2array, tree2array, rec2array

import argparse
import vars as v 
from getRatio import getRatio

def plotDistribution(vars, X_train, y_train, w_train,class_weight, dir,show=False):
    
    bkg_weights = np.empty(X_train[y_train==0].shape[0])
    sig_weights = np.empty(X_train[y_train==1].shape[0])
    
    bkg_weights.fill(class_weight[0])
    bkg_weights *= w_train[y_train==0].reshape(-1)
    sig_weights.fill(class_weight[1])
    sig_weights *= w_train[y_train==1].reshape(-1)

    for i,var in enumerate(v.variables):
        fig = plt.figure()
        ax = fig.add_subplot(111)
        ax.hist(X_train[:,i][y_train==1], bins=20, alpha=0.5,color='b', weights=sig_weights)
        ax.hist(X_train[:,i][y_train==0], bins=20, alpha=0.5,color='r', weights=bkg_weights)
        plt.xlabel(var)
        plt.savefig(dir+'plots/' + var + '.png')
        plt.savefig(dir+'plots/' + var + '.pdf')
        if show: plt.show(block=False)
        plt.close(fig)

    return
    
def plotROC(y_test, score, filename, dir, show=False):
    '''
    Plot ROC curve for a given test sample
    '''
    fpr, tpr, _ = roc_curve(y_test, score)
    roc_auc = auc(fpr, tpr)

    print('ROC AUC = %f' % roc_auc)

    fpr = 1.0 - fpr

    plt.grid(color='gray', linestyle='--', linewidth=1)
    plt.plot(tpr, fpr, label='Shallow NN, area = %0.2f' % roc_auc)
    plt.plot([0, 1], [1, 0], linestyle='--', color='black', label='Luck')
    plt.xlabel('Signal acceptance')
    plt.ylabel('Background rejection')
    plt.title('Receiver operating characteristic')
    plt.xlim(0, 1)
    plt.ylim(0, 1)
    plt.xticks(np.arange(0, 1, 0.1))
    plt.yticks(np.arange(0, 1, 0.1))
    plt.legend(loc='lower left', framealpha=1.0)
    
    dir = dir+'/plots/'
    dir = dir.replace('//','/')
    plt.savefig(dir + filename + '.png')
    plt.savefig(dir + filename + '.pdf')
    
    if show: plt.show()
    
    plt.clf()
    return

def plotOutputScore(X_test, y_test, test_score,w_test, X_train, y_train, train_score, w_train, class_weight, filename, dir, binning, show=False):
    '''
    Plot output score for given test and train sample
    '''
    
    test_train_ratio = X_train.shape[0]/float(X_test.shape[0])
    print(X_test.shape[0])
    print(X_train.shape[0])

    print(test_train_ratio)

    
    bkg_train_weights = np.empty(X_train[y_train==0].shape[0])
    sig_train_weights = np.empty(X_train[y_train==1].shape[0])
    bkg_train_weights.fill(class_weight[0])
    bkg_train_weights *= w_train[y_train==0].reshape(-1)
    sig_train_weights.fill(class_weight[1])
    sig_train_weights *= w_train[y_train==1].reshape(-1)

    bkg_test_weights = np.empty(X_test[y_test==0].shape[0])
    sig_test_weights = np.empty(X_test[y_test==1].shape[0])
    bkg_test_weights.fill(class_weight[0])
    bkg_test_weights *= w_test[y_test==0].reshape(-1)
    bkg_test_weights[:,] *= test_train_ratio
    sig_test_weights.fill(class_weight[1])
    sig_test_weights *= w_test[y_test==1].reshape(-1)
    sig_test_weights[:,] *= test_train_ratio

    bkg_train_weights = bkg_train_weights.reshape(-1, 1)
    sig_train_weights = sig_train_weights.reshape(-1, 1)
    bkg_test_weights = bkg_test_weights.reshape(-1, 1)
    sig_test_weights = sig_test_weights.reshape(-1, 1)

    ratio = False
    fig = plt.figure(figsize=(8,6))
    if ratio:
        ax1 = plt.subplot2grid((4,4), (0,0), colspan=4, rowspan=3)
        #ax1.xaxis.set_ticks([])
    else: 
        ax1 = plt.subplot2grid((4,4), (0,0), colspan=4, rowspan=4)
    ax1.tick_params(direction='in')
    ax1.set_xlim((binning[1], binning[2]))
    ax1.xaxis.set_ticks_position('both')
    ax1.yaxis.set_ticks_position('both')

    s_histTrain, s_binsTrain, s_patchesTrain = plt.hist(train_score[y_train==1],weights=sig_train_weights, histtype='stepfilled', color='r', label='Signal (Training)', alpha=0.5, bins=binning[0], range=(binning[1], binning[2]))
    b_histTrain, b_binsTrain, b_patchesTrain = plt.hist(train_score[y_train==0],weights=bkg_train_weights, histtype='stepfilled', color='b', label='Background (Training)', alpha=0.5, bins=binning[0], range=(binning[1], binning[2]))
    
    s_histTest, s_binsTest = np.histogram(test_score[y_test==1], weights=sig_test_weights, bins=binning[0], range=(binning[1], binning[2]))
    b_histTest, b_binsTest = np.histogram(test_score[y_test==0], weights=bkg_test_weights, bins=binning[0], range=(binning[1], binning[2]))

    width = (s_binsTrain[1] - s_binsTrain[0])
    center = (s_binsTrain[:-1] + s_binsTrain[1:]) / 2
    plt.errorbar(center, s_histTest, fmt='o', c='r', label='Signal (Testing)') # TODO define yerr = sqrt( sum w^2 ) per bin!
    plt.errorbar(center, b_histTest, fmt='o', c='b', label='Background (Testing)') # TODO define yerr = sqrt( sum w^2 ) per bin!


    plt.xlabel('Output score')
    plt.ylabel('a.u.')
    #plt.title('Receiver operating characteristic')
    #plt.xlim(0, 1)
    #plt.ylim(0, 1)
    #plt.xticks(np.arange(0, 1, 0.1))
    #plt.yticks(np.arange(0, 1, 0.1))
    plt.legend(loc='upper right', framealpha=1.0)
    #plt.yscale('log')
    
    if ratio:
        ax2 = plt.subplot2grid((4,4), (3,0), colspan=4, rowspan=1)
        getRatio(s_histTest, s_binsTest, b_histTest, b_binsTest, 'b')
        ax2.set(xlabel='Output score', ylabel='S/B')
        ax2.set_xlim((binning[1],binning[2]))
        ax2.set_ylim((0,2))
        ax2.grid()
        ax2.tick_params(direction='in')
        ax2.xaxis.set_ticks_position('both')
        ax2.yaxis.set_ticks_position('both')

    dir = dir+'/plots/'
    dir = dir.replace('//','/')

    plt.savefig(dir + filename + '.png')
    plt.savefig(dir + filename + '.pdf')
    
    if show: plt.show()
    
    plt.clf()
    return

def plotAccuracy(history,dir,filename):
    
    plt.plot(history.history['acc'])
    plt.plot(history.history['val_acc'])
    plt.title('model accuracy')
    plt.ylabel('accuracy')
    plt.xlabel('epoch')
    plt.legend(['train', 'test'], loc='upper left')

    dir = dir+'/plots/'
    dir = dir.replace('//','/')

    plt.savefig(dir + filename + '.png')
    plt.savefig(dir + filename + '.pdf')
    #plt.show()
    
    plt.clf()
    return
    
def plotLoss(history,dir,filename):
    
    plt.plot(history.history['loss'])
    plt.plot(history.history['val_loss'])
    plt.title('model loss')
    plt.ylabel('loss')
    plt.xlabel('epoch')
    plt.legend(['train', 'test'], loc='upper left')
    
    dir = dir+'/plots/'
    dir = dir.replace('//','/')

    plt.savefig(dir + filename + '.png')
    plt.savefig(dir + filename + '.pdf')
    #plt.show()
    
    plt.clf()
    return