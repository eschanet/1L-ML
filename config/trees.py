from collections import namedtuple

background = [{'name': 'ttbar', 'file': 'ttbar.npy'},
              {'name': 'ttv', 'file': 'ttv.npy'},
              {'name': 'wjets', 'file': 'wjets.npy'},
              {'name': 'zjets', 'file': 'zjets.npy'},
              {'name': 'dijets', 'file': 'dijets.npy'},
              {'name': 'diboson', 'file': 'diboson.npy'},
              {'name': 'multiboson', 'file': 'multiboson.npy'},
              {'name': 'singletop', 'file': 'singletop.npy'}
              ]

signal = [{'name' : 'C1C1_WW_200p0_80p0', 'file':'C1C1_WW_200p0_80p0.npy'},
          {'name' : 'C1C1_WW_250p0_60p0', 'file':'C1C1_WW_250p0_60p0.npy'},
          {'name' : 'C1C1_WW_300p0_0p0', 'file':'C1C1_WW_300p0_0p0.npy'},
          {'name' : 'C1C1_WW_400p0_0p0', 'file':'C1C1_WW_400p0_0p0.npy'},
          {'name' : 'C1C1_WW_500p0_0p0', 'file':'C1C1_WW_500p0_0p0.npy'},
          {'name' : 'C1N2_WZ_200p0_50p0', 'file':'C1N2_WZ_200p0_50p0.npy'},
          {'name' : 'C1N2_WZ_240p0_110p0', 'file':'C1N2_WZ_240p0_110p0.npy'},
          {'name' : 'C1N2_WZ_250p0_50p0', 'file':'C1N2_WZ_250p0_50p0.npy'},
          {'name' : 'C1N2_WZ_300p0_50p0', 'file':'C1N2_WZ_300p0_50p0.npy'},
          {'name' : 'C1N2_WZ_500p0_0p0', 'file':'C1N2_WZ_500p0_0p0.npy'}
          ] 
