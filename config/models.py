from collections import namedtuple

Algorithm = namedtuple("Algorithm", "name modelname options")

# define your ML algorithm here
analysis = [
Algorithm('NN',
          'DNN_ADAM_layer3x32_batch100_NormalInitializer_dropout0p0_binary',
          {
          'layers':[32,32,32],
          'epochs':20,
          'batchSize':100,
          'dropout':0,
          'optimizer':'adam',
          'activation':'relu',
          'initializer':'normal',
          'loss':'binary_crossentropy'
          }
),
]
 
