# README


Small collection of scripts used for ML R&D for the electroweak 1L analysis.

### Getting started
Setup conda environment:
```
source setup.sh
```
This sets up my conda environment. If you have your own environment, exchange the paths in the setup.sh file with your paths.

### Transform .root files
In order to use python based frameworks for ML applications, the ROOT trees need to be transformed into numpy arrays. Fortunately, this can be easily done with root_numpy
```
./convertTrees.py
```

### Train and test neural net
Train and then test a DNN and plot some debug plots.
```
./trainModel.py
```


