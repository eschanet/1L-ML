echo "Setting up environment on etp..."
export PATH=/project/etp5/eschanet/packages/miniconda3/bin:$PATH

CondaDir="/project/etp5/eschanet/packages/miniconda3"

export WorkDir=`pwd`
export PYTHONPATH=$PYTHONPATH:$WorkDir/python
export PYTHONPATH=$PYTHONPATH:$WorkDir/config
# export PATH=$PATH:$WorkDir/scripts

source activate ml27
source "$CondaDir/envs/ml27/bin/thisroot.sh"

